"""mai_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from mai_app import views

urlpatterns = [
    path('products/<int:pk>/', views.ProductDetailView.as_view(), name='product_detail'),
    path('mai_app/', include('mai_app.urls')),
    path('admin/', admin.site.urls),
    path('', views.login_view, name='login'),
    path('login', views.login_view, name='login'),
    path('menu', views.menu, name='menu'),
    path('logout', views.logout_view, name='logout'),
    path('products/', views.product_list, name='product_list'),
    path('add_to_basket/', views.add_to_basket, name='add_to_basket'),
    path('delete/<int:product_id>/', views.delete_from_basket, name='delete_from_basket'),
    path('basket/', views.basket_view, name='basket'),
    path('checkout/', views.checkout_view, name='checkout'),
    path('signup/', views.sign_up_view, name='signup'),
    path('success/', views.success, name='success'),
    path('update_basket/<int:product_id>/', views.update_basket, name='update_basket'),
]
