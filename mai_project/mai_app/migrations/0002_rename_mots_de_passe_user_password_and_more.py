# Generated by Django 4.1 on 2023-01-21 21:18

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mai_app', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='user',
            old_name='mots_de_passe',
            new_name='password',
        ),
        migrations.RenameField(
            model_name='user',
            old_name='nom',
            new_name='username',
        ),
    ]
