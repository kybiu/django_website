from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import User


class Customer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    email = models.EmailField(default="")


class Product(models.Model):
    id = models.IntegerField(primary_key=True)
    qty = models.IntegerField()
    unit_price = models.FloatField()
    appellation = models.TextField()
    illustration = models.TextField()


class Order(models.Model):
    user = models.ForeignKey(Customer, on_delete=models.CASCADE)
    products = models.ManyToManyField(Product)
    total_amount = models.DecimalField(max_digits=10, decimal_places=2)
    shipping_address = models.CharField(max_length=255)
    billing_address = models.CharField(max_length=255)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=20, default='pending')
