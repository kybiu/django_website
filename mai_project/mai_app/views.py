from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate as djangoAuthenticate
from django.contrib.auth import login
from django.contrib.auth import logout
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic import DetailView

from .forms import CustomerSignUpForm
from .models import Product, Order, Customer

debug = getattr(settings, 'DEBUG', False)


def index(request):
    return HttpResponse("")


def login_view(request):
    if debug:
        print("login")
        print(request.POST)
    if 'username' not in request.POST or 'password' not in request.POST:
        error = "Entrez le nom d'utilisateur et le mot de passe"
        return render(request, 'login.html', {'error': error})
    username = request.POST['username']
    password = request.POST['password']
    if debug:
        print("username " + username)
        print("password " + password)
    user = djangoAuthenticate(username=username, password=password)
    if user is not None:
        if debug:
            print(request.session)
            # request.session['logged_user'] = username
            login(request, user)
        return render(request, 'menu.html')
    else:
        error = "nom d'utilisateur ou mot de passe incorrect"
        return render(request, 'login.html', {'error': error})


def menu(request):
    print("menu")
    return render(request, 'menu.html')


def logout_view(request):
    print("logout")
    logout(request)
    return redirect('login')


def product_list(request):
    products = Product.objects.all()
    context = {'products': products}
    return render(request, 'product_list.html', context)


def add_to_basket(request):
    if request.method == 'POST':
        product_id = request.POST.get('product_id')
        quantity = request.POST.get('quantity')
        product = Product.objects.get(id=product_id)
        basket = request.session.get('basket', {})
        if int(quantity) > product.qty:
            messages.error(request, 'Quantity not available')
            return redirect('product_detail', product_id)

        if product_id in basket:
            basket[product_id] += int(quantity)
        else:
            basket[product_id] = int(quantity)
        request.session['basket'] = basket

        # return redirect('basket')
        return redirect(request.META.get('HTTP_REFERER'))

    else:
        return render(request, 'product_list.html')


def delete_from_basket(request, product_id):
    basket = request.session.get('basket', {})
    if str(product_id) in basket.keys():
        del basket[str(product_id)]
        request.session['basket'] = basket
    return redirect(request.META.get('HTTP_REFERER'))


def basket_view(request):
    basket = request.session.get('basket', {})
    products = Product.objects.filter(id__in=basket.keys())
    basket_items = []
    for product in products:
        basket_items.append({
            'product': product,
            'quantity': basket[str(product.id)],
        })
    return render(request, 'basket.html', {'basket_items': basket_items})


def checkout_view(request):
    if request.method == 'POST':
        basket = request.session.get('basket', {})
        basket_items = []
        total_amount = 0
        for product_id, quantity in basket.items():
            product = Product.objects.get(id=product_id)
            total_amount += product.unit_price * int(quantity)
            basket_items.append({'product': product, 'quantity': quantity})

        shipping_address = request.POST.get('shipping_address')
        billing_address = request.POST.get('billing_address')
        if shipping_address is None or billing_address is None:
            return render(request, 'checkout_view.html', {'basket_items': basket_items, 'total_amount': total_amount})

        order = Order.objects.create(
            user=request.user.customer,
            total_amount=total_amount,
            shipping_address=shipping_address,
            billing_address=billing_address
        )

        for item in basket_items:
            order.products.add(item['product'])
            item['product'].qty -= item['quantity']
            item['product'].save()

        request.session['basket'] = {}
        return redirect('success')
    else:
        basket = request.session.get('basket', {})
        basket_items = []
        total_amount = 0
        for product_id, quantity in basket.items():
            product = Product.objects.get(id=product_id)
            total_amount += product.unit_price * int(quantity)
            basket_items.append({'product': product, 'quantity': quantity})
        return render(request, 'checkout_view.html', {'basket_items': basket_items, 'total_amount': total_amount})


def sign_up_view(request):
    if request.method == 'POST':
        form = CustomerSignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()  # load the profile instance created by the signal

            new_customer = Customer(user=user)

            new_customer.email = form.cleaned_data.get('email')
            new_customer.save()
            return redirect('login')
    else:
        form = CustomerSignUpForm()
    return render(request, 'signup.html', {'form': form})


def success(request):
    return render(request, 'success.html')


def update_basket(request, product_id):
    # Retrieve the product from the database
    product = Product.objects.get(id=product_id)

    # Retrieve the new quantity from the request
    new_quantity = int(request.POST.get(f'quantity_{product.id}'))

    # Check if the new quantity is valid
    if new_quantity <= 0:
        messages.error(request, "Quantity must be greater than zero.")
    elif new_quantity > product.qty:
        messages.error(request, "Quantity exceeds stock.")
    else:
        # Retrieve the basket from the session
        basket = request.session.get('basket', {})

        # Update the quantity of the product in the basket
        basket[product_id] = new_quantity

        # Save the updated basket to the session
        request.session['basket'] = basket

        messages.success(request, "Basket updated.")

    # Redirect back to the basket page
    return redirect('basket')


class ProductDetailView(DetailView):
    model = Product
    template_name = 'product_detail.html'
    context_object_name = 'product'
